#!/bin/bash

#SBATCH --time=400:00:00
#SBATCH --cpus-per-task=16
#SBATCH --partition=all
#SBATCH -J IceNuclei
#SBATCH --exclude=node0,node1
#SBATCH -o /home/ecpl/marchatz/models/TM4-ECPL/logs/run_tm4-%j.out
#SBATCH -e /home/ecpl/marchatz/models/TM4-ECPL/logs/run_tm4-%j.err
#SBATCH --no-requeue

OMP_NUM_THREADS=16

ulimit -s unlimited
cd $HOME/models/TM4-ECPL
module purge

module load intel
module load HDF
module load Szip
#-------------------------------Run SETUP---------------------------------------
## #SBATCH --exclude=node0,node1

# script main name:
mprog='run-tm4'

# main project name:
mproj='Ice_Marine_Dust'

# simulation number:
sim='2.1'

# secondary project name:
proj="${mproj}_${sim}"

# grid resolutions:
res='glb3x2' ; nlev='60' ; nlev2='25'

##

#res='glb6x4' ; nlev='60' ; nlev2='34'
#res='glb6x4' ; nlev='91' ; nlev2='34'
#res='glb9x6' ; nlev='60' ; nlev2='34'

# begin year of run
start_year=2010

# begin month in begin year
start_month=1

# begin day in begin month in begin year
start_day=1

# begin hour on begin day. 0h is at start of the day
start_hour=0

# end year of run
end_year=2012

# end month in end year
end_month=1

# end day in end month in end year
end_day=1

# end hour on end day. 0h is at start of the day
end_hour=1

# Choose fire emission database to be used
# choices are:
# - ACCMIP
# - ACCMIP85 ! Only for 2050 and 2100
# - GFEDv2
# - GFEDv3
# - GFEDv3_IIASA
# - GUESS-ES
# - ZERO_fires # turn off fire emissions
# - FINN
fire_emissions='ACCMIP'

# Choose anthropogenic emissions database to be used
# choices are:
# - ACC25P
# - ACCMIP85 ! Only for 2050 and 2100
# - IIASA
# - IIASA_CLE
# - IIASA_MFR
anthropogenic='ACCMIP'

# Choose biogenic emissions database to be used
# choices are:
# - MEGAN_MACC
biogenic='MEGAN_MACC'

# Choose vertical ditribution of fire emissions
# choices are
# - dentener
# - surface
distr='dentener'


# Choose Mineral DATASET 
# choises are
# nickovic   !Nickovic et al., 2012
# journet    !psd_ecearth_journet-case2-dominant (maria concalves)
# claquin    !psd_ecearth_claquin51-dominant (maria concalves)
mineralogy='journet' 




emis_year=0 #affect ACCMIP ANTHRO and BB emissions
#------------------------END RUN SETUP------------------------------------------
homedir=`pwd`

bindir="${homedir}/bin"

sourcedir="${homedir}/src"

#inputdir="/data/eternus/TM/INPUT"
inputdir="${homedir}/input"

#outputdir='/data/x4/nick/tm4-out'
outputdir="${homedir}/out"
#outputdir="/data/x4/marchatz/out"

# overwrite runid set in runscript by passing following
runid="tm4_${res}_ml${nlev}_${nlev2}_${proj}"

# create runs directory if necessary:
runsdir="${homedir}/runs/${runid}"

#create script directory inside runsdir
scriptdir="${runsdir}/bin"

# Check if runs directories exist
test ! -d runs  &&  /bin/mkdir runs
test ! -d ${runsdir}  &&  /bin/mkdir ${runsdir}
test ! -d ${scriptdir}  &&  /bin/mkdir ${scriptdir}

# copy standard scripts to run directory preserve times etc:
echo "$prog - copy scripts and source to run directory ..."
/bin/cp -rf ${bindir}/* ${runsdir}/bin/.
# also copy src dir to run directory for verification:
/bin/cp -rf ${sourcedir} ${runsdir}/.

# define the file which contains the start field
#saveold_initial="${outputdir}/save.hdf201001"
#saveold_initial="/data/x6/mariak/TM4ECPLout/PANOPLY/NUTRIENTS/tm4_glb3x2_ml60_34_PANOPLY_NUTRIENTS_BASE/save.hdf200912"
#saveold_initial="/home/ecpl/marchatz/models/TM4-ECPL/out/tm4_glb3x2_ml60_34_Quartz_with_Sedim_2011_0.3/save.hdf201101"
#saveold_initial="/home/ecpl/marchatz/models/TM4-ECPL/out/tm4_glb3x2_ml60_34_SpinUp_0.0/save.hdf201106"

#saveold_initial="/home/ecpl/marchatz/TM4_25levels/25save.hdf200901"
#saveold_initial="/data/x4/marchatz/out/tm4_glb3x2_ml60_25_Paper_25_Lvls2008-2013_1.0/save.hdf201212"
#saveold_initial="/home/ecpl/marchatz/online_version_ICE/25saveRas.hdf201303"
saveold_initial='/data/x4/marchatz/out/tm4_glb3x2_ml60_25_Claquin_Miner_1.0/save.hdf201007'
#saveold_initial='/data/x4/marchatz/out/tm4_glb3x2_ml60_25_Journet_2.0/save.hdf201009'
# script name:
prog="${mprog}-${res}-ml${nlev}-${nlev2}-${proj}"

# name of runscript (in 'scripts' directory):
runscript='tm4.sc'

# --- settings --------------------

# overwrite rcfile set in runscript by passing following
# name as second argument (not always accepted by script ...)
#rcfile='tm4-tm4meteo.rc'
rcfile='tm4-tm5meteo.rc'

# macro's defined:
#defines='with_hdf'
defines='with_hdf tm5meteo'

# expand macro definitions:
#
# Use FPP to specify model resolution according to grid resolutions
#   o -D{res} e.g run on glb1x1/glb3x2/glb6x4
#   o -Dmodel_lev_${nlev2} e.g run on 25/34 levels
#   o -Dml_{nlev} e.g run with ECMWF meteorology in 60(ERA-INTERIM / OD)  or 91 (only OD) vertical levels
#
# Specify additional definitions
#   o -Dwith_daily_output : produce hdf daily outputs (-Dwith_statistics has to be enabled)
#   o -Dwith_second_m : runs with second_momnts interopolation (slopes have to be enabled)
#   o -Dsave_zero : starts with zero saveold concentrations
#   o -Dwith_station_column : produce hdf output files for specific coordinates (all levels)
#     o -Dwith_column_budget : write budget terms in the station file
#   o -Dwith_fluxtend: caclulates advection tendencies per model's glidbox
#   o -Dwithout_budgets: without budget calculations
#   o -Dwithout_emission: without emissions
#   o -Dwithout_GLYocean_emissions: without GLY ocean emissions
#   o -Dwithout_gaschem: without gas-phase chemistry calculations
#   o -Dwithout_aqchem: without aqueous-phase chemistry calculations
#   o -Dwithout_partchem: without aqueous-phase chemistry calculations
#   o -Dwithout_hetchem: without heterogeneous chemistry calculations
#   o -Dwithout_photolysis: without photolysis calculations
#   o -Dwithout_boundary: without boundary concditions (i.e. O3, CH4 and HNO3)
#     o -Dwithout_o3_nudging : only without O3 nudging
#     o -Dwithout_ch4_nudging : only without CH4 nudging
#     o -Dwithout_hno3_nudging : only without HNO3 nudging
#     o -Dwith_GLYocean_nudging : only with GLY ocean nudging
#   o -Dwithout_radcalc: without RF/AOD calculations
#   o -Dwithout_sedimentation: without sedimentation
#   o -Dwith_4d_restart_file : old saveold 4D start up
#   o -Dwith_daily_output :
#   o -Dwith_24h_output  :
#   o -Dwith_station_output
#   o -Dwith_Jmineral : Mineralogy from Journet et al., 1999

ppflags="-D${res} -Dml_${nlev} -Dmodel_lev_${nlev2} -Dwithout_radcalc"
##-Dwith_daily_output -Dwith_station_column"
ompflags="-qopenmp"
makedefs=''

# remove object files (T|F) ?
# also the objects from the file_hdf, tmm etc modules (T|F) ?
clean=T
clean_all=T


# --- begin -----------------------

/bin/cat << EOF
$prog -
$prog - ==================================================
$prog - ===                  TM4                       ===
$prog - ===              ECPL version                  ===
$prog - ===         Chemistry Transport Model          ===
$prog - ===  contact:  mariak@chemistry.uoc.gr         ===
$prog - ===  http://ecpl.chemistry.uoc.gr/kanakidou/   ===
$prog - ===        Last Update on 25/10/13             ===
$prog - ==================================================
$prog -
$prog - Start at `/bin/date`
$prog -
EOF

# goto source directory:

cd $sourcedir

# clean:
if [ "${clean}" = "T" ]; then
  echo "$prog - clean ..."
  /bin/touch dummy.o
  for fo in `/bin/ls *.o` ; do
    f=`basename ${fo} '.o'`
    case ${f} in
      go* | parray* | file_hdf* | binas* | num* | phys* | grid* | tmm* )
        test "${clean_all}" = "T"  &&  /bin/rm -f ${f}.{o,mod}
        ;;
      * )
        /bin/rm -f ${f}.{o,mod}
        ;;
    esac
  done # .o files
  echo "$prog - "
fi  # clean

# make
for def in ${defines} ; do
  makedefs="${makedefs}${def}${pp}=T "
  ppflags="${ppflags} -D${def} "
  echo $ppflags
done

:
# run Makedepf90
makedepf90 ${ppflags} -o tm4.x *.[fF]* > Makefile_deps
echo "makedepf90 ${ppflags} -o tm4.x *.[fF]* > Makefile_deps"
# compile
echo "$prog - make ..."
echo " "
gmake -j  tm4.x FPP="${ppflags} ${ompflags}"
#make tm4.x FPP="${ppflags}"
echo " "
echo "$prog - "

# back ...
cd $homedir

# setup input directory if necessary ...
if [ ! -d 'input' ]; then
  #echo "link input directory ..."
  #/bin/ln -s -f /home/steliosm/TM4/input .
  echo "$prog - no 'input' directory found ..."
  exit 1
fi

# run
cd $scriptdir
./${runscript} ${res} ${nlev} ${nlev2} ${start_year} ${start_month} ${start_day} ${start_hour} ${end_year} ${end_month} ${end_day} ${end_hour} ${homedir} ${sourcedir} ${inputdir} ${outputdir} ${scriptdir} ${runsdir} ${saveold_initial} ${runid} ${rcfile} ${fire_emissions} ${anthropogenic} ${biogenic} ${distr} ${emis_year} ${mineralogy}


# --- end --------------------------

/bin/cat << EOF
$prog -
$prog - End at `/bin/date`
$prog -
EOF
